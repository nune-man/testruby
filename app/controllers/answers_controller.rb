Class AnswersController << ApplicationController

def create
  @question = Question.find(params[:questions_id])
  @answer = @question.answer.build(answer_params)
  @answer.user = current_user
  @answer.save
  redirect_to questions_url
end

def destroy
  @question = Question.find(params[:questions_id])
  @answer = @question.answer.find(params[:id]).destroy
  redirect_to questions_url
end

private

def answer_params
  params.required(:answer).permit(:content)
end