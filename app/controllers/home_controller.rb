class HomeController < BaseController
  # load_and_authorize_resource
  #
  # before_action  only: [:new, :create]
  def index
    if user_signed_in?
      redirect_to questions_path
    end
  end
end
