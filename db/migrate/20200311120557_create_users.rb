class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :username
      t.string :password_digest
      t.string :position
      t.boolean :active, default: true
      t.references :privilege, index: true
      t.timestamps
    end
  end
end
